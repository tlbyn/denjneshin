fastapi
sqlalchemy
uvicorn
alembic
asyncpg
psycopg2-binary
python-dotenv
python-decouple
pyjwt
bcrypt
passlib
phonenumbers
greenlet
pydantic
sqlmodel
redis
geopy
kavenegar
email-validator