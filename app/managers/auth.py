from typing import Optional

import jwt
from datetime import datetime, timedelta
from fastapi import HTTPException, Request
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials

from app import settings

class AuthManager:
    @staticmethod
    def encode_token(user: object, exp_days: int, type: str):
        try:
            payload = { 
                "sub": user.id,
                "exp": datetime.utcnow() + timedelta(days=exp_days),
                "type": type
            }
            return jwt.encode(payload, settings.secret_key, algorithm="HS256")
        except Exception as ex:
            #log ex
            raise ex
    @staticmethod
    def decode_token(token: str):
        return jwt.decode(token, settings.secret_key, algorithms=["HS256"]) 

class JWTBearer(HTTPBearer):
    def __init__(self, auto_error: bool = True):
        super(JWTBearer, self).__init__(auto_error=auto_error)

    async def __call__(self, request: Request):
        credentials: HTTPAuthorizationCredentials = await super(JWTBearer, self).__call__(request)
        if credentials:
            if not credentials.scheme == "Bearer":
                raise HTTPException(status_code=403, detail="Invalid authentication scheme.")
            if not self.verify_jwt(credentials.credentials):
                raise HTTPException(status_code=403, detail="Invalid token or expired token.")
            return credentials.credentials
        else:
            raise HTTPException(status_code=403, detail="Invalid authorization code.")

    def verify_jwt(self, jwtoken: str) -> bool:
        isTokenValid: bool = False

        try:
            payload = AuthManager.decode_token(jwtoken)
        except:
            payload = None
        if payload:
            isTokenValid = True
        return isTokenValid