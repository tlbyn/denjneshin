from fastapi import HTTPException
from fastapi import status as http_status
from sqlalchemy import delete, select
from sqlmodel.ext.asyncio.session import AsyncSession

from .auth import AuthManager
from ..models import Token, User


class TokenManager:

    def __init__(self, session:AsyncSession):
        self.session = session

    async def get(self, user: User) -> Token | None:
        stmnt = select(
            Token
        ).where(
            Token.user_id == user.id
        )
        results = await self.session.execute(stmnt)

        return results.scalar_one_or_none()
    
    async def create(self, user: User) -> Token:
        access_token = AuthManager.encode_token(user, 1, "access")
        refresh_token = AuthManager.encode_token(user, 30, "refresh")

        token_val = {
            "user_id": user.id,
            "access_token": access_token,
            "refresh_token": refresh_token
        }
        token = Token(**token_val)
        self.session.add(token)
        await self.session.commit()
        await self.session.refresh(token)
        return token

    async def refresh(self, user: User) -> Token:
        access_token = AuthManager.encode_token(user, 1, "access")
        refresh_token = AuthManager.encode_token(user, 30, "refresh")

        token = self.get(user)
        val = {
            "user_id": user.id,
            "access_token": access_token,
            "refresh_token": refresh_token
        }

        for k, v in val.items():
            setattr(token, k, v)

        self.session.add(token)
        await self.session.commit()
        await self.refresh(token)

        return token

    