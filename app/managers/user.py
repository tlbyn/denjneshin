from fastapi import HTTPException
from fastapi import status as http_status
from sqlalchemy import delete, select
from sqlmodel.ext.asyncio.session import AsyncSession


from ..models import User, UserVerify, UserBase


class UserManager:

    def __init__(self, session:AsyncSession):
        self.session = session


    async def get_by_phone_number(self, phone_number: str) -> User | None:
        stmnt = select(
            User
        ).where(
            User.phone_number == phone_number
        )
        results = await self.session.execute(stmnt)

        return results.scalar_one_or_none()

    
    async def register(self, data: UserVerify) -> User | None:
        values = data.dict()

        values.pop("code")
        user = User(**values)
        self.session.add(user)
        await self.session.commit()
        await self.session.refresh(user)
        return user

    async def get_by_id(self, user_id: str) -> User | None:
        stmnt = select(
            User
        ).where(
            User.id == user_id
        )
        results = await self.session.execute(stmnt)

        return results.scalar_one_or_none()
