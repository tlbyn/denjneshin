from asyncpg import UniqueViolationError
from fastapi import HTTPException
from ..db import database
from ..models import Service, ServiceProfile

class ServiceManager:
    @staticmethod
    async def create(service_data):
        try:
            id_ = await database.execute(Service.insert().values(**service_data))
        except UniqueViolationError:
            raise HTTPException(400, "Service with this servicename already exist")
        service_do = await database.fetch_one(Service.select().where(Service.c.id == id_))
        return service_do

    @staticmethod
    async def get_services_by_location(province: str, city: str) -> List[Service]:
        query = select(Service).join(ServiceProfile).where(
            ServiceProfile.province == province,
            ServiceProfile.city == city
        )
        services = []
        async with database:
            async with database.transaction():
                result = await database.fetch_all(query)
                for row in result:
                    services.append(Service.from_orm(row))
        return services