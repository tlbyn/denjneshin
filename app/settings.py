from pydantic import BaseSettings


class Settings(BaseSettings):
    # Base
    debug: bool
    project_name: str
    version: str
    description: str

    #postgres
    postgres_host: str
    postgres_user: str
    postgres_password: str
    postgres_db: str
    postgres_port: str

    #redis
    redis_host: str
    redis_port: int
    redis_db: int

    #jwt
    secret_key: str

    #apis
    kavenegar_apikey: str