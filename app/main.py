from fastapi import FastAPI

from .routes import api_router
from . import settings
from .models.health import HealthCheck

app = FastAPI(
    title=settings.project_name,
    version=settings.version,
    debug=settings.debug
)

@app.get("/", response_model=HealthCheck, tags=["status"])
async def health_check():
    return {
        "name": settings.project_name,
        "version": settings.version,
        "description": settings.description
}

app.include_router(api_router)

# @app.on_event("startup")
# async def startup():
    # await database.connect()


# @app.on_event("shutdown")
# async def shutdown():
#     await database.disconnect()