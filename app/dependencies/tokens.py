from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.db import get_async_session

from app.managers.token import TokenManager

async def get_token_manager(
    session: AsyncSession = Depends(get_async_session)
) -> TokenManager:
    return TokenManager(session=session)