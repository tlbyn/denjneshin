from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.db import get_async_session

from app.managers.user import UserManager

async def get_user_manager(
    session: AsyncSession = Depends(get_async_session)
) -> UserManager:
    return UserManager(session=session)