import random
import string
from .db import redis
from .models.users import UserVerify

def generate_code(phone_number: str) -> str:
    code = "".join(random.choices(string.digits, k=4))  # generate a 6-digit code
    redis.set(phone_number, code, ex=120)  # cache the code for 2 minutes
    return code

def verify_code(data: UserVerify) -> bool:
    values = data.dict()
    cached_code = redis.get(values["phone_number"])
    if cached_code != values["code"]:
        return False
    redis.delete(values["phone_number"])
    return True