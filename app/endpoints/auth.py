import jwt
from fastapi import APIRouter, Depends, status as http_status
from ..managers.user import UserManager
from ..managers.token import TokenManager
from ..managers.auth import AuthManager
from ..utils import generate_code, verify_code
from ..models.users import UserBase, UserVerify, User, TokenBase, TokenRefresh
from ..dependencies.users import get_user_manager
from ..dependencies.tokens import get_token_manager
from app import settings

import kavenegar

from ..models.users import Token 

router = APIRouter(tags=["Auth"])

@router.post("/send_sms")
async def send_sms(data: UserBase):
    phone_number = data.dict()["phone_number"]

    code = generate_code(phone_number)
    #send sms with kavenegar
    # try:
    #     api = kavenegar.KavenegarAPI(settings.kavenegar_apikey)
    #     params = {
    #         'receptor': phone_number,
    #         'template': 'denji',
    #         'token': code,
    #         'type': 'sms',
    #     }
    #     response = api.verify_lookup(params)
    #     print(response)
    # except kavenegar.APIException as e: 
    #     raise e
    # except kavenegar.HTTPException as e: 
    #     raise e
    return {"message":code}


@router.post(
    "/verify_code",
    response_model=TokenBase,
    status_code=http_status.HTTP_201_CREATED
)
async def verify_code(
    data: UserVerify,
    users: UserManager = Depends(get_user_manager),
    tokens: TokenManager = Depends(get_token_manager)
):
    values = data.dict()
    if not verify_code(data=data):
        return {"message": "Invalid code."}

    user = await users.get_by_phone_number(values.get("phone_number"))
    
    if user is None:
        user = await users.register(data=data)
    
    token = await tokens.get(user)

    if token is None:
        token = await tokens.create(user)
        
        return token

    token = await tokens.refresh(user)
    return token


@router.post("/refresh_token", response_model=TokenBase)
async def refresh_token(
    data: TokenRefresh,
    users: UserManager = Depends(get_user_manager),
    tokens: TokenManager = Depends(get_token_manager)
):
    values = data.dict()
    payload = AuthManager.decode_token(values.get("refresh_token"))
    user = await users.get_by_id(payload.get("sub"))
    if user is None:
        return {"message": "refresh token is not right"}
    token_ = await tokens.get(user)

    if token_ is None:
        return {"message": "refresh token is not right"}
    
    if token_.refresh_token != values.get("refresh_token"):
        return {"message": "refresh token is not right"}
    
    return await tokens.refresh(user)


    



