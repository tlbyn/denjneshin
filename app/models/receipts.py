from datetime import datetime
import uuid as uuid_pkg

from typing import Optional, List


from sqlmodel import Field, SQLModel, DateTime, Enum, Relationship
from sqlalchemy import text, Column


from .enums import receipt_status



""" Association tables """
class ItemMemberLink(SQLModel, table=True):
    user_id: Optional[int] = Field(
        default=None, foreign_key="user.id", primary_key=True
    )
    item_id: Optional[int] = Field(
        default=None, foreign_key="receiptitem.id", primary_key=True
    )


class ReceiptMemberLink(SQLModel, table=True):
    user_id: Optional[int] = Field(
        default=None, foreign_key="user.id", primary_key=True
    )
    receipt_id: Optional[uuid_pkg.UUID] = Field(
        default=None, foreign_key="receipt.id", primary_key=True
    )
    share: float
    is_paid: bool = False
    #relations
    user: "User" = Relationship(back_populates="receipt_links")
    receipt: "Receipt" = Relationship(back_populates="user_links")

""" tables"""

class Receipt(SQLModel, table=True):
    id: uuid_pkg.UUID = Field(
        default_factory=uuid_pkg.uuid4,
        primary_key=True,
        index=True,
        nullable=False,
        sa_column_kwargs={
            "server_default": text("gen_random_uuid()"),
            "unique": True
       }
    )
    title: Optional[str]
    avatar: Optional[str]
    status: str = Field(
        sa_column = Column(
            "role",
            receipt_status,
            nullable=False
        ),
        default="open"
    )
    balance: float = 0
    amount: float
    discount: float =0
    table_id: Optional[int] = Field(default=None, foreign_key="table.id")
    table: Optional["Table"] = Relationship(back_populates="receipts")

    receiptitems: List["ReceiptItem"] = Relationship(back_populates="receipt")

    user_links: List[ReceiptMemberLink] = Relationship(back_populates="receipt")


class ReceiptItem(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    number: int = 1
    price: float
    amount: float
    receipt_id: Optional[uuid_pkg.UUID] = Field(default=None, foreign_key="receipt.id")
    receipt: Optional["Receipt"] = Relationship(back_populates="receiptitems")

    item_id: Optional[int] = Field(default=None, foreign_key="item.id")
    item: Optional["Item"] = Relationship(back_populates="receiptitems")

    members: List["User"] = Relationship(back_populates="receiptitems", link_model=ItemMemberLink)


class Transaction(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    user_id: int
    service_id: int 
    amount: float
    date: datetime = Field(
        default_factory=datetime.utcnow,
        nullable=False,
        sa_column_kwargs={
            "server_default": text("current_timestamp(0)")
        }
   )
