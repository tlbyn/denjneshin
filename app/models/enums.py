from sqlalchemy.databases import postgres
from sqlalchemy import event
from sqlmodel import SQLModel

user_role = postgres.ENUM(
    "user",
    "super_admin",
    "admin",
    "staff",
    "marketer",
    "marketer_supervisor",
    name="user_role"
)

category_service = postgres.ENUM(
    "cafe",
    "fastfood",
    "cafe_restaurant",
    "restaurant",
    "vegan_restaurant",
    "italian_restaurant",
    "catering",
    "other_nationalities_restaurant",
    "ashkade",
    name="category_service"
)


smoke_option_without = postgres.ENUM(
    "cant_smoke",
    "can_smoke",
    name="smoke_option_without"
)

smoke_option_with = postgres.ENUM(
    "cant_smoke",
    "can_smoke",
    "partly",
    name="smoke_option_with"
)


receipt_status = postgres.ENUM(
    "open",
    "awaiting_confirmation",
    "paid",
    "close",
    name="receipt_status"
)


bank_option = postgres.ENUM(
    "blu",
    "saman",
    "mellat",
    name="bank_option"
)


@event.listens_for(SQLModel.metadata, "before_create")
def _create_enums(metadata, conn, **kw):  # noqa: indirect usage
   user_role.create(conn, checkfirst=True)