from datetime import datetime
from typing import Optional, List



from sqlmodel import Field, SQLModel, DateTime, Enum, Relationship, Column as sqlCol
from sqlalchemy import UniqueConstraint, text, Column


from .enums import user_role
from .receipts import ItemMemberLink, ReceiptMemberLink


class UserBase(SQLModel):
    phone_number: str


class UserVerify(UserBase):
    code: str


class User(UserBase, table=True):
    __table_args__ = (UniqueConstraint("phone_number"),)

    id: Optional[int] = Field(default=None, primary_key=True)
    name: Optional[str]
    created_at: datetime = Field(
        default_factory=datetime.utcnow,
        nullable=False,
        sa_column_kwargs={
            "server_default": text("current_timestamp(0)")
        }
    )
    role: Optional[str] = Field(
        sa_column = Column(
            "role",
            user_role,
            nullable=True
        )
    )
    lat: Optional[str]
    lon: Optional[str]
    province_id: Optional[int]
    city_id: Optional[int]



    #relation
    userprofile: Optional["UserProfile"] = Relationship(
        sa_relationship_kwargs={'uselist': False},
        back_populates="user"
    )


    wallet: Optional["Wallet"] = Relationship(
        sa_relationship_kwargs={'uselist': False},
        back_populates="user"
    )


    receiptitems: List["ReceiptItem"] = Relationship(back_populates="members", link_model=ItemMemberLink)

    receipt_links: List[ReceiptMemberLink] = Relationship(back_populates="user")




class UserProfile(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    natid: Optional[str]
    bd_date: Optional[datetime] = Field(
        sa_column=sqlCol(
            DateTime(timezone=True),
            default=None
        )
    )
    avatar: Optional[str]
    denjneshin_id: Optional[int]
    denjneshin_lastupdate: Optional[datetime] = Field(
        sa_column=sqlCol(
            DateTime(timezone=True),
            default=None
        )
    )
    email: Optional[str]
    email_verified: bool = Field(default=False)

    #relations
    user_id: Optional[int] = Field(default=None, foreign_key="user.id")
    user: Optional["User"] = Relationship(back_populates="userprofile")



class Wallet(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    balance = float = 0


    #relations
    #relations
    user_id: Optional[int] = Field(default=None, foreign_key="user.id")
    user: Optional["User"] = Relationship(back_populates="wallet")



class Province(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str
    slug: str 


class City(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str
    slug: str 
    province_id: int



class Deposite(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    user_id: int 
    amount: int 
    transaction_id: str 
    date: datetime = Field(
        default_factory=datetime.utcnow,
        nullable=False,
        sa_column_kwargs={
            "server_default": text("current_timestamp(0)")
        }
   )

class TokenRefresh(SQLModel):
    refresh_token: Optional[str]

class TokenBase(TokenRefresh):
    access_token: Optional[str]

class Token(TokenBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    user_id: Optional[int]




