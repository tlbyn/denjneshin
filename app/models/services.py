from datetime import datetime

from typing import Optional, List

from sqlmodel import Field, SQLModel, Column as sqlCol, DateTime, Enum, Relationship
from sqlalchemy import UniqueConstraint, Column, text

from .enums import category_service, smoke_option_without, smoke_option_with, bank_option


class Service(SQLModel, table=True):
    __table_args__ = (UniqueConstraint("servicename"),)
    id: Optional[int] = Field(default=None, primary_key=True)
    servicename: str
    name: str
    marketer_id: int
    balance: int = 0
    denjneshin_num: int = 0
    rate: int = 0
    category: str = Field(
        sa_column = Column(
            "role",
            category_service,
            nullable=True
        ),
        default="cafe"
    )
    certificate_img: str 
    slug: str
    created_at: datetime = Field(
        default_factory=datetime.utcnow,
        nullable=False,
        sa_column_kwargs={
            "server_default": text("current_timestamp(0)")
        }
   )

    #relations
    serviceprofile: Optional["ServiceProfile"] = Relationship(
        sa_relationship_kwargs={'uselist': False},
        back_populates="service"
    )

    serviceowner: Optional["ServiceOwner"] = Relationship(
        sa_relationship_kwargs={'uselist': False},
        back_populates="service"
    )

    positions: List["Position"] = Relationship(back_populates="service")

    sections: List["Section"] = Relationship(back_populates="service")

    categories: List["Category"] = Relationship(back_populates="service")

    rates: List["SRate"] = Relationship(back_populates="service")


class ServiceProfile(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    province_id: int 
    city_id: int 
    address: str = Field(max_length=2000)
    lat: str 
    lon: str
    about: Optional[str] = Field(max_length=1000)
    instagram: Optional[str]
    whatsapp: Optional[str]
    telegram: Optional[str]
    phone_number: Optional[str]
    tel_number: Optional[str]
    website: Optional[str]
    denjneshin_benefits: Optional[str] = Field(max_length=1000)
    smoke_option: str = Field(
        sa_column = Column(
            "smoke_option_with",
            smoke_option_with,
            nullable=True
        ),
        default="can_smoke"
    )
    avatar: Optional[str]
    header: Optional[str]

    #relations
    service_id: Optional[int] = Field(default=None, foreign_key="service.id")
    service: Optional["Service"] = Relationship(back_populates="serviceprofile")


class ServiceOwner(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    owner_id: int
    home_address: str 
    shaba: str
    bank_account_num: str 
    credit_card_num: str 
    bank : str = Field(
        sa_column = Column(
            "bank_option",
            bank_option,
            nullable=True
        ),
        default="blu"
    )

    #relations
    service_id: Optional[int] = Field(default=None, foreign_key="service.id")
    service: Optional["Service"] = Relationship(back_populates="serviceowner")


class Position(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str
    #booleans
    #relations
    service_id: Optional[int] = Field(default=None, foreign_key="service.id")
    service: Optional["Service"] = Relationship(back_populates="positions")


class Section(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str
    description: str
    smoke_option: str = Field(
        sa_column = Column(
            "smoke_option_without",
            smoke_option_without,
            nullable=True
        ),
        default="can_smoke"
    )
    maximum_reserve: int

    #relations
    service_id: Optional[int] = Field(default=None, foreign_key="service.id")
    service: Optional["Service"] = Relationship(back_populates="sections")

    tables: List["Table"] = Relationship(back_populates="section")


class Table(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str 
    has_open_receipt: bool = False
    capacity: int


    #relations
    section_id: Optional[int] = Field(default=None, foreign_key="section.id")
    section: Optional["Section"] = Relationship(back_populates="tables")


    receipts: List["Receipt"] = Relationship(back_populates="table")


class Category(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str
    description: str


    #relations
    service_id: Optional[int] = Field(default=None, foreign_key="service.id")
    service: Optional["Service"] = Relationship(back_populates="categories")


    items: List["Item"] = Relationship(back_populates="category")


class Item(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str 
    price: float 
    discount: float
    ingredients: str = Field(max_length=2000)
    image: str 
    rate: float = 0


    #relations
    category_id: Optional[int] = Field(default=None, foreign_key="category.id")
    category: Optional["Category"] = Relationship(back_populates="items")

    rates: List["IRate"] = Relationship(back_populates="item")

    receiptitems: List["ReceiptItem"] = Relationship(back_populates="item")
    

class SRate(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    rate: float
    submitted_at: datetime = Field(
        default_factory=datetime.utcnow,
        nullable=False,
        sa_column_kwargs={
            "server_default": text("current_timestamp(0)")
        }
   )
    user_id: int

    #relations
    service_id: Optional[int] = Field(default=None, foreign_key="service.id")
    service: Optional["Service"] = Relationship(back_populates="rates")
    

class IRate(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    rate: float 
    submitted_at: datetime = Field(
        default_factory=datetime.utcnow,
        nullable=False,
        sa_column_kwargs={
            "server_default": text("current_timestamp(0)")
        }
   )
    user_id: int


    #relations
    item_id: Optional[int] = Field(default=None, foreign_key="item.id")
    item: Optional["Item"] = Relationship(back_populates="rates")


class Image(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    image: str 
    description: str
    service_id: int


class Withdraw(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    service_id: int 
    amount: float
    transaction_id: str 
    requested_at: datetime = Field(
        default_factory=datetime.utcnow,
        nullable=False,
        sa_column_kwargs={
            "server_default": text("current_timestamp(0)")
        }
   )
    paid_at: Optional[datetime] = Field(
        sa_column=sqlCol(
            DateTime(timezone=True),
            default=None
        )
    )
